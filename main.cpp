
#include <stdlib.h>
#include <stdio.h>

#include <QApplication>
#include <QObject>
#include <QPixmap>
#include <QSplashScreen>
#include <QTimer>
#include <QPen>
#include <QColor>
#include <QFont>
#include <QEventLoop>

#include <QFile>
#include <QDir>
#include <QScopedPointer>
#include <QTextStream>
#include <QDateTime>
#include <QLoggingCategory>

#include "mainwindow.h"

// Smart pointer to log file
QScopedPointer<QFile>   m_logFile;

//
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

int main(int argc, char *argv[]) {

    /* Do some intitial compiler and platform checks */
#if defined(_MSC_VER) || defined(_MSC_FULL_VER) || defined(_MSC_BUILD)
#error "Wrong compiler or platform!"
#endif

    /* avoid surprises! */
    if ((sizeof(char)      != 1) ||
       (sizeof(short)      != 2) ||
       (sizeof(int)        != 4) ||
       (sizeof(long long)  != 8) ||
       (sizeof(float)      != 4) ||
       (sizeof(double)     != 8)) {
        fprintf(stderr, "Wrong compiler or platform!\n");
        return EXIT_FAILURE;
    }

#if defined(__LP64__)
    if (sizeof(long) != 8) {
        fprintf(stderr, "Wrong compiler or platform!\n");
        return EXIT_FAILURE;
    }
#else
    if (sizeof(long) != 4) {
        fprintf(stderr, "Wrong compiler or platform!\n");
        return EXIT_FAILURE;
    }
#endif

#if defined(__MINGW64__)
    if (sizeof(long) != 4) {
        fprintf(stderr, "Wrong compiler or platform!\n");
        return EXIT_FAILURE;
    }
#endif

#if QT_VERSION >= 0x050600
    QCoreApplication::setAttribute(Qt::AA_DisableHighDpiScaling);
#endif

    /* Setup the app */
    QApplication app(argc, argv);

    // Open the log file to record to
    QString log_name = "ui.log";
    QString log_location = "/home/dan/teetacsi/EDFbrowser/logs/Recording2/Recording/";
    QString log_file = log_location + log_name;
    m_logFile.reset(new QFile(log_file));
    m_logFile.data()->open(QFile::WriteOnly | QFile::Text);
    QTextStream out(m_logFile.data());
    out << "[" << endl;
    out.flush();
    qInstallMessageHandler(messageHandler);

    // Create the splash screen
    QPixmap pixmap(":/images/splash.png");
    QPainter p(&pixmap);
    QFont sansFont("Noto Sans", 10);
    p.setFont(sansFont);
    p.setPen(Qt::black);

    if (!strcmp(PROGRAM_BETA_SUFFIX, "")) {
        p.drawText(250, 260, 300, 30, Qt::AlignLeft | Qt::TextSingleLine, "version " PROGRAM_VERSION "    " THIS_APP_BITS_W);
    }
    else {
        p.drawText(50, 240, 300, 30, Qt::AlignLeft | Qt::TextSingleLine, "version " PROGRAM_VERSION "  " THIS_APP_BITS_W);
        p.drawText(50, 260, 300, 30, Qt::AlignLeft | Qt::TextSingleLine, PROGRAM_BETA_SUFFIX);
    }

    QSplashScreen splash(pixmap, Qt::WindowStaysOnTopHint);
    QTimer t1;
    t1.setSingleShot(true);

#if QT_VERSION >= 0x050000
    t1.setTimerType(Qt::PreciseTimer);
#endif

    QObject::connect(&t1, SIGNAL(timeout()), &splash, SLOT(close()));

    // Show the splash screen and start
    if (QCoreApplication::arguments().size() < 2) {
        splash.show();
        t1.start(3000);
        QEventLoop evlp;
        QTimer::singleShot(100, &evlp, SLOT(quit()));
        evlp.exec();
    }

    // Run the app
    qApp->setStyleSheet("QMessageBox { messagebox-text-interaction-flags: 5; }");
    class UI_Mainwindow MainWindow;
    return app.exec();
}


// The implementation of the handler
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {

    QTextStream out(m_logFile.data());

    out << "\n{\n  ";
    out << "\"Timestamp\": \"" << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz") << "\",";

    // At the moment we're only using the qDebug level to log UI changes
//    switch (type)
//    {
//    case QtInfoMsg:     out << "INF "; break;
//    case QtDebugMsg:    out << "RCD "; break; // Use debug message to record UI changes to log file
//    case QtWarningMsg:  out << "WRN "; break;
//    case QtCriticalMsg: out << "CRT "; break;
//    case QtFatalMsg:    out << "FTL "; break;
//    }
    // Write to the output category of the message and the message itself
//    out << context.category << ": "
//        << msg << endl;
    out << msg << endl;
    out.flush();    // Clear the buffered data
}



