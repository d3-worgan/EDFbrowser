# This Python file uses the following encoding: utf-8
import os
import json

import cv2

print("Drawing some lines...")

# Get the working directory
# - Either from the command line, otherwise ask for user input - but explain they could have also used the command line
# - Another option is to read the logs folder and offer a list of folders in the logs foler to choose from
# Create a second directory
# Go into the first directory
# Open the log file
# Iterate the entries in the log file
# Find the corresponding png for each entry
# Make a copy of the png, use the coordinates in the log to draw 
# the predicted lines on the image
# Then save the altered image to the second directory
# Continue for each entry in the log file

# Create a second directory
# - First check the directory doesnt exist
# - If it does then delete it and make a new one
# - If it doesnt then make a new one
# - Check the new directory has been created
working_directory = "/home/dan/teetacsi/EDFbrowser/logs/Recording1/"
recording_directory = os.path.join(working_directory, "Recording")
results_directory = os.path.join(working_directory, 'Results')
if os.path.exists(os.path.join(working_directory, 'Results')):
    os.rmdir(results_directory)
    os.mkdir(results_directory)
else:
    os.path.join(results_directory)
assert os.path.exists(results_directory), "Something wen wrong creating the results directory"


# Open the log file
# - Open the file
# - Append [] to fix for json format
# - Load the file into dict using json loader

log_file_name = os.path.join(recording_directory, "ui.log")
assert os.path.exists(log_file_name), "Couldnt find a ui.log file"

ui_state = {}
lines = []
with open(log_file_name, 'r') as log_file:
    lines = log_file.readlines()

if '},' in lines[-2]:
    lines[-2] = '}'
    with open(log_file_name, 'w') as log_file:
        log_file.writelines(lines)
else:
    print("Nothing to see here")


with open(log_file_name, 'r') as log_file:
    ui_state = json.load(log_file)

# Iterate the entries in the log file
files = next(os.walk(recording_directory))[2]

# Find the corresponding png for each entry
# - Check there are the same amount of pngs as entries in the list
# - Check if the id of the entry is in the name of the file
#       - If it is then load it into an opencv image
print(files)
for log in ui_state:
    print(log['id'])
    for f in files:
        if str(log['id']) in f:
            print(f"Draw the lines for log {log['id']} on pic {f}")

# Draw the lines on the image
# - Draw a line between top left and top right
# - draw a line between top right and bottom right
# - Draw a line between bottom right bottom left
# - Draw a line between bottom left and top left
# - For each channel
#       - Draw line between left and right at channel y coordinate
# - Check if the timestring is minutes, seconds, milliseconds, microseconds
# - Then draw lines at every minute, second, millisecond, microsend

# Save the altered image to the second directory

# Continue for entry in the log file