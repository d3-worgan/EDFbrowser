"""
Use this script to confirm that the UI state logs are working correctly.

The UI logger currently records the bounding box of the graph, the time on the left x_limit,
the the amount of time displayed on the graph e.g. 10 seconds (TODO: find the y positions of the channels)

Each time a parameter is changed it is logged into a ui.log file with the other ui state parameters.
Each time a log entry is made a screenshot is taken and saved into the same folder.

To confirm the logs are tracking the UI correctly we can use the logs to trace lines over the screenshots

This produces a new folder next to the input directory the added visualisations.
"""


import os
import json
import argparse
import re
import cv2


"""
Clean or create a new directory to store the results in
"""
def setup_directories(working_directory):
    assert os.path.exists(working_directory), f"{working_directory} is not a valid directory or does not exist."
    recording_directory = os.path.join(working_directory, "Recording")
    results_directory = os.path.join(working_directory, 'Results')
    if os.path.exists(results_directory):
        files = next(os.walk(results_directory))[2]
        if len(files) > 0:
            proceed = input(f"Do you want to overwrite the data in {results_directory}? y/n")
            if proceed == 'y':
                files = next(os.walk(results_directory))[2]
                for f in files:
                    os.remove(os.path.join(results_directory, f))
                os.rmdir(results_directory)
                os.mkdir(results_directory)
            else:
                raise FileExistsError
    else:
        os.mkdir(results_directory)
    assert os.path.exists(recording_directory), "Couldnt find the recording directory"
    assert os.path.exists(results_directory), "Something wen wrong creating the results directory"
    return recording_directory, results_directory


"""
The output ui log from the Qt program is not valid JSON so we need to clean up any "artifacts"
"""
def fix_ui_output(log_file_name):
    lines = []
    with open(log_file_name, 'r') as log_file:
        lines = log_file.readlines()
    if '},' in lines[-2]:
        lines[-2] = '}'
        with open(log_file_name, 'w') as log_file:
            log_file.writelines(lines)


"""
Load the UI state logs into a dictionary 
"""
def load_logs(log_file_name):
    assert os.path.exists(log_file_name), "Couldnt find a ui.log file"
    ui_state = {}
    with open(log_file_name, 'r') as log_file:
        ui_state = json.load(log_file)
    return ui_state


"""
Draw the visualisation on the screenshots that were taken during runtime.
(Confirms the logs are matching the events on the screen)
"""
def draw_lines(logs, files, results_directory):
    for log in logs:
        for f in files:
            if str(log['id']) in f:
                print(f"Draw the lines for log {log['id']} on pic {f}")
                rfilepath = os.path.join(recording_directory, f)
                image = cv2.imread(rfilepath)
                if image is not None:
                    viewtime_string = log['viewtime_string']
                    print(f"View time string {viewtime_string}")
                    start_time = viewtime_string[viewtime_string.find("(")+1:viewtime_string.find(")")]
                    print(f"start_time {start_time}")
                    page_time = log['pagetime']
                    print(f"Page time {page_time}")
                    page_time = page_time / 10000000
                    print(f"Pagetime fixed {page_time}")
                    top_left = (log['maincurve_geometry']['top-left'][0], log['maincurve_geometry']['top-left'][1])
                    top_right = (log['maincurve_geometry']['top-right'][0], log['maincurve_geometry']['top-right'][1])
                    bottom_left = (log['maincurve_geometry']['bottom-left'][0], log['maincurve_geometry']['bottom-left'][1])
                    bottom_right = (log['maincurve_geometry']['bottom-right'][0], log['maincurve_geometry']['bottom-right'][1])

                    graph_width = top_right[0] - top_left[0]


                    line_color = (0, 255, 0)
                    line_width = 3
                    image = draw_graph_boundary(image, top_left, top_right, bottom_left, bottom_right, line_color, line_width)
                    image = draw_vertical_seconds(image, start_time, page_time, graph_width, top_left, bottom_left, line_color, line_width)
                    wfilepath = os.path.join(results_directory, f)
                    cv2.imwrite(wfilepath, image)
                else:
                    print(f"Couldnt load the file at {f}")


"""
Draws the graphs bounding box
"""
def draw_graph_boundary(image, top_left, top_right, bottom_left, bottom_right, line_color, line_width):
    image = cv2.line(image, top_left, top_right, line_color, line_width)
    image = cv2.line(image, bottom_left, bottom_right, line_color, line_width)
    image = cv2.line(image, top_left, bottom_left, line_color, line_width)
    image = cv2.line(image, top_right, bottom_right, line_color, line_width)
    return image


"""
Draws some vertical bars describing the x scale and timestamps
"""
def draw_vertical_seconds(image, start_time, time_scale, graph_width, top_left, bottom_left, line_color, line_width):
    x_step = graph_width / (time_scale + 1)
    line_bottom = bottom_left
    line_top = top_left
    for i in range(int(time_scale)):
        image = cv2.line(image, line_bottom, line_top, line_color, line_width)
        line_bottom = (int(line_bottom[0] + x_step), int(line_bottom[1]))
        line_top = (int(line_top[0] + x_step), int(line_top[1]))
    return image


"""
User needs to specify the location of the ui.log and associated screenshots
"""
def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", required=True, help="Specify a directory containing the ui.log and corresponding images to draw on")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    recording_directory, results_directory = setup_directories(args.input)
    log_file_name = os.path.join(recording_directory, "ui.log")
    fix_ui_output(log_file_name)
    logs = load_logs(log_file_name)
    files = next(os.walk(recording_directory))[2]
    draw_lines(logs, files, results_directory)
